﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeilausPeli
{
   public class KeilausTestiPeli
    
    {
        static void Main(string[] args)
        { }

        private int[] rolls = new int[21];
        private int currentRoll = 0;

            public int Score
        {
            get
            {
                var score = 0;
                var rollIndex = 0;
                for (var frame = 0; frame < 10; frame++)
                {
                    if (rolls[rollIndex] == 10)
                    {
                        score += rolls[rollIndex] + rolls[rollIndex + 1] + rolls[rollIndex + 2];
                        rollIndex++;
                    }
                    else if (IsSpare(rollIndex))
                    {


                        score += GetSpareScore(rollIndex);
                        rollIndex += 2;
                    }
                    else
                    {
                        score += GetStandardScore(rollIndex);
                        rollIndex += 2;
                    }
                }
                return score;
                
            }
        }

        private int GetStandardScore(int rollIndex)
        {
            return rolls[rollIndex] + rolls[rollIndex + 1];
        }

        private int GetSpareScore(int rollIndex)
        {
            return rolls[rollIndex] + rolls[rollIndex + 1] + rolls[rollIndex + 2];
        }

        private bool IsSpare(int rollIndex)
        {
            return rolls[rollIndex] + rolls[rollIndex + 1] == 10;
        }

        public void Roll(int pins)
        {
            rolls[currentRoll++] = pins;
            

        }

        }
    }

