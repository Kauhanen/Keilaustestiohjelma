﻿using KeilausPeli;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeilausTestit
{[TestFixture]
    class keilaustest
    {

        static void Main(string[] args)
        { }
        [Test]
        public void HeitäNollapistettä()
        {
            var game = new KeilausTestiPeli();
            RollMany(game,0,20);
            Assert.AreEqual(0, game.Score);

        }

        public void RollMany(KeilausTestiPeli game, int pins, int rolls)
        {
            for (int i = 0; i < rolls; i++)

                game.Roll(pins);
        }

        [Test]
        public void HeitäKaikillaKaksi()
        {
            var game = new KeilausTestiPeli();

            RollMany(game, 2, 20);
               
                Assert.AreEqual(40, game.Score);
            
        }
        [Test]
        public void KaatoKahdellaHeitolla()
        {
            var game = new KeilausTestiPeli();


            game.Roll(5);
            game.Roll(5);
            game.Roll(3);
            RollMany(game, 0, 17);
            Assert.AreEqual(16, game.Score);
        }

        [Test]
        public void Kaato()
        {
            var game = new KeilausTestiPeli();

            game.Roll(10);
            game.Roll(3);
            game.Roll(4);
            RollMany(game,0, 16);
            Assert.AreEqual(24, game.Score);
        }
        [Test]
        public void TäydetPisteet()
        {
            var game = new KeilausTestiPeli();

            RollMany(game, 10, 12);
            Assert.AreEqual(300, game.Score);
        }
       


    }

}

