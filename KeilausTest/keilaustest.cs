﻿using keilaus;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeilausTest
{
    [TestFixture]
   public class keilaustest
    {
        Frame frame;

        [Test]
        public void CreateFrame()
        {
            frame = new Frame();
        }

        [Test]
        public void AllZero()
        {
            RepeatSameRoll(pins: 0, count: 20);
            ExpectScoreToBe(0);
        }

        [Test]
        public void AllOne()
        {
            RepeatSameRoll(pins: 1, count: 20);
            ExpectScoreToBe(20);
        }

        [Test]
        public void OneSpare()
        {
            Roll(8, 2, 9, 0);
            RepeatSameRoll(pins: 0, count: 16);
            ExpectScoreToBe(8 + 2 + 9 * 2);
        }

        [Test]
        public void OneStrike()
        {
            Roll(10, 6, 2);
            RepeatSameRoll(pins: 0, count: 16);
            ExpectScoreToBe(10 + 6 * 2 + 2 * 2);
        }

        [Test]
        public void PerfectGame()
        {
            RepeatSameRoll(pins: 10, count: 12);
            ExpectScoreToBe(300);
        }

        private void ExpectScoreToBe(int expectedScore)
        {
            int score = frame.GetScore();
            Assert.AreEqual(expectedScore, score);
        }

        private void Roll(params int[] pins)
        {
            foreach (var pin in pins)
            {
                frame.Roll(pin);
            }
        }

        private void RepeatSameRoll(int pins, int count)
        {
            for (int i = 0; i < count; i++)
            {
                frame.Roll(pins);
            }
        }
    }
}